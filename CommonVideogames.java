/**
 * @fecha 5/5/2021
 * @seccion Algoritmos y Estructuras de Datos 20 
 * @class CommonVideogames
*/ 
[
	#Filtrar los videojuegos por título 
	inputId = videogames_df[videogames_df['title'].isin(inputVideogames['title'].tolist())]
	#Luego fusionándolo para que podamos obtener el videogamesId. Lo está fusionando implícitamente por título. 
	inputVideogames = pd.merge(inputId, inputVideogames)
	#Eliminar información que no usaremos del dataframe
	inputMovies = inputVideogames.drop('year', 1)
	#Final input dataframe
	inputMovies
	#Filtrar a los usuarios que han jugado videojuegos que la entrada ha visto y almacenarlas 
	userSubset = ratings_df[ratings_df['videogameId'].isin(inputVideogames['videogameId'].tolist())]
	userSubset.head()
	# Groupby crea varios sub dataframes de datos donde todos tienen el mismo valor en la columna especificada #como parámetro 
	userSubsetGroup = userSubset.groupby(['userId'])
	# Ordenamos por los usuarios que han puntuado mas a los videojuegos en común con la entrada
	userSubsetGroup = sorted(userSubsetGroup, key=lambda x: len(x[1]), reverse=True)
	userSubsetGroup = userSubsetGroup[0:100]
]
