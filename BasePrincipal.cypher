//lista de usuarios
CREATE (Dariel:Usuario {idUsuario: 1, nombre: 'Dariel Villatoro'})
CREATE (Jonathan:Usuario {idUsuario: 2, nombre: 'Jonathan Villatoro'})

//lista de videojuegos
CREATE (DMC5:Juego {idJuego: 1, nombreJuego: 'Devil May Cry 5', genero: 'Accion-Aventura', calificacionPopular: 0, calificacionCritica: 0})
CREATE (GoWPS4:Juego {idJuego: 2, nombreJuego: 'God of War (PS4)', genero: 'Accion-Aventura', calificacionPopular: 0, calificacionCritica: 0})
CREATE (FNV:Juego {idJuego: 3, nombreJuego: 'Fallout: New Vegas', genero: 'Juego de Rol', calificacionPopular: 0, calificacionCritica: 0})
CREATE (SMO:Juego {idJuego: 4, nombreJuego: 'Super Mario Odyssey', genero: 'Plataformas', calificacionPopular: 0, calificacionCritica: 0})

//lista de generos
CREATE (AA:Genero {id: 1, nombreGenero: 'Accion-Aventura'})
CREATE (RPG:Genero {id: 2, nombreGenero: 'Juego de Rol'})
CREATE (PLATAFORMAS: Genero {id: 3, nombreGenero: 'Platformas'})

//conexiones de nodos
(Dariel)-(:LIKES)->(RPG)
(Ariel)-(:LIKES)->(AA)

(DMC5)-[:IS]->(AA)
(GoWPS4)-[:IS]->(AA)
(FNV)-[:IS]->(RPG)
(SMO)-[:IS]->(PLATAFORMAS)