/**
 * @fecha 5/5/2021
 * @seccion Algoritmos y Estructuras de Datos 20 
 * @class DataFrameUsuario
*/ 

pearsonCorrelationDict = {}
#Para cada grupo de usuarios de nuestro subconjunto 
for name, group in userSubsetGroup:
    #Comencemos ordenando la entrada y el grupo de usuarios actual para que los valores no se mezclen más adelante. 
    group = group.sort_values(by='movieId')
    inputVideogames = inputVideogames.sort_values(by='movieId')
    #Obtenga las puntuaciones de las reseñas de los videojuegos que ambos tienen en común 
    temp_df = inputVideogames[inputVideogames['videogameId'].isin(group['videogameId'].tolist())]
    #Y luego guárdelos en una variable de búfer temporal en un formato de lista para facilitar cálculos futuros 
    tempRatingList = temp_df['rating'].tolist()
    #Pongamos también las reseñas del grupo de usuarios actual en un formato de lista 
    tempGroupList = group['rating'].tolist()
    data_corr = {'tempGroupList': tempGroupList,
            'tempRatingList': tempRatingList}
    pd_corr = pd.DataFrame(data_corr)
    r = pd_corr.corr(method="pearson")["tempRatingList"]["tempGroupList"]
    #ahora eliminamos los nan de nuestro coef de pearson
    if math.isnan(r) == True:
        r = 0
    pearsonCorrelationDict[name] = r
#Convertimos el diccionario a un dataframe:         
pearsonDF = pd.DataFrame.from_dict(pearsonCorrelationDict, orient='index')
pearsonDF.columns = ['similarityIndex']
pearsonDF['userId'] = pearsonDF.index
pearsonDF.index = range(len(pearsonDF))
pearsonDF.head()