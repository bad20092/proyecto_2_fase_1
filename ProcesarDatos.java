/**
 * @fecha 5/5/2021
 * @seccion Algoritmos y Estructuras de Datos 20 
 * @class ProcesarDatos
*/ 

	#creamos una columna nueva que se llamará year
videogames_df['year'] = videogames_df.title.str.extract('(\(\d\d\d\d\))',expand=False)
	#Quitando los paréntesis 
videgames_df['year'] = videogames_df.year.str.extract('(\d\d\d\d)',expand=False)
	#Eliminar los años de la columna "título" y el espacio
videogames_df['title'] = videogames_df.title.str.replace(' (\(\d\d\d\d\))', '')
	#eliminamos los generos
videogames_df = videogames_df.drop('genres',1)
